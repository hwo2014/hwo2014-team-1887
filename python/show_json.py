
import json
import pprint
import os
import sys

if len(sys.argv) == 2:
    file = sys.argv[1]
    interactive = False

else:
    files = []
    for file in os.listdir("."):
        if file.endswith(".json"):
            files.append(file)
    print ""
    print "Available files:"
    print ""
    for n in xrange(0, len(files)):
        print str(n).rjust(5), '-', files[n]
    print ""
    index = raw_input("Which file? ")
    if index == '':
        exit()
    file = files[int(index)]
    interactive = True

#######################################################################################################################
# Read and output the file

print "Showing file", file

pp = pprint.PrettyPrinter(indent=4)

json_file = open(file, 'rb')
contents = json_file.read()
json_file.close()

data = json.loads(contents)

print "-" * 72
print pp.pformat(data)
print "-" * 72

if interactive:
    raw_input("Press enter to close")
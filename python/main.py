import importlib
import socket
import sys
import logging

logging.basicConfig(level=logging.DEBUG,
                    format='[%(levelname)s] (%(threadName)-10s %(name)-29s) %(message)s',
)

# define the available packages and use the first one as the default
packages = ['FastBot', 'NoobBot']

num_args = len(sys.argv)

if num_args < 4:
    print "Usage:"
    print "\t./run host port botname botkey [create|join] [cars] [track] [password] [package]"

logging.debug("Num Args = " + str(num_args))

host, port, name, key = sys.argv[1:5]
start_type = "join"
cars = 1
track = None
password = None
package = packages[0]

#start_type (create/join)
if num_args > 5 and not sys.argv[5] is None:
    start_type = sys.argv[5]
#cars (number of cars per race)
if num_args > 6 and not sys.argv[6] is None:
    cars = sys.argv[6]
#track ((optional)string name of race track)
if num_args > 7 and not sys.argv[7] is None:
    track = sys.argv[7]
#password ((optional)string password of track)
if num_args > 8 and not sys.argv[8] is None:
    cars = sys.argv[8]
#package (string name of bot to use)
if num_args > 9 and not sys.argv[9] is None:
    if sys.argv[10] in packages:
        package = sys.argv[9]
    else:
        package = packages[0]

logging.info(
    "Connecting with: package={0}, host={1}, port={2}, name={3}, key={4}, start type={5}, track={6}, password={7}, cars={8}".format(
        package, host, port, name, key, start_type, track, password, cars))

bot_module = importlib.import_module(".main", package)
logging.info("imported: {0}".format(bot_module))

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((host, int(port)))
logging.info("connected to: {0}".format(s.getpeername()))

bot = bot_module.Bot(s, name, key)
if num_args == 5:
    bot.run()
else:
    bot.advanced_run(start_type, track, password, cars)
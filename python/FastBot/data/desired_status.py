class DesiredStatus:

    def __init__(self, bot):

        # init logging
        import logging
        self.logger = logging.getLogger(__name__)

        self.bot = bot

        self.debug = True

        self.attributes = ['speed', 'lane']

        self.speed = None
        self.lane = None

    def is_same_as(self, other):
        for attr_name in self.attributes:
            if self.is_attr_same_as(other, attr_name):
                if self.debug:
                    a = getattr(self, attr_name)
                    b = getattr(other, attr_name)
                    self.logger.debug('{0} {1} != {0} {2}'.format(attr_name, a, b))
                return False
        return True

    def is_attr_same_as(self, other, attr_name):

        a = getattr(self, attr_name)
        b = getattr(other, attr_name)

        return self.both_values_present_differ(a, b)

    def both_values_present_differ(self, a, b):
        if self.both_values_not_none(a, b) and a != b:
            return True
        return False

    def both_values_not_none(self, a, b):
        if not a is None and not b is None:
            return True
        return False

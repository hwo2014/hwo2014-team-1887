# Import standard python libraries
import importlib
import threading
import time

# Import project packages/modules
from ControlLayer import serverinterface, botstate

# Import bot specific modules and packages
from .data import RaceStatus, DesiredStatus


class Bot(object):

    def __init__(self, socket, name, key):
        """ Construct the bot object by defining the start values for some attributes
        :param socket:
        :param name:
        :param key:
        :return:
        """

        self.worker_modules = ['receive_track', 'maximum_speed', 'collision_detector', 'drift_handler']
        self.workers = []
        self.worker_ticks = []

        self.events_subscriptions = {}

        self.running = False
        self.joined = False
        self.tick = 0
        self.tick_lock = threading.Lock()
        self.on_tick_done = threading.Event()
        self.on_tick_done.set()

        # init logging
        import logging
        logging.basicConfig( level=logging.DEBUG,
                            format='[%(levelname)s] (%(threadName)-10s %(name)-29s) %(message)s',
                            )
        self.logger = logging.getLogger(__name__)

        #
        self.bot_state = botstate.botState(self, name, key, 'None')
        self.competitor_states = {}
        self.server_interface = serverinterface.ServerInterface(socket, self, self.bot_state, self.competitor_states)
        self.race_status = RaceStatus(self)
        self.desired_status = None

        self.logger.debug('bot init completed')

    def advanced_run(self, start_type, track, password, cars):
        """ Allow the bot to be started with the 'advanced joining' system so that it
             can join test servers alongside other bots
        :param start_type:
        :param track:
        :param password:
        :param cars:
        :return:
        """
        if self.running:
            self.logger.error('cant run - already running')
            return

        self.logger.debug('advanced_run called')

        if not self.joined:
            if start_type != 'join':
                self.server_interface.do_create_race(cars, track, password)
            else:
                self.server_interface.do_join_race_advanced(cars, track, password)

            self.joined = True

        self.run()

    def run(self):
        """ Start the bot by initialising background threads and starting the message loop running
        :return:
        """
        if self.running:
            self.logger.error('cant run - already running')
            return

        self.running = True

        self.logger.debug('run called')

        if not self.joined:
            self.server_interface.do_simple_join()

        for worker_module in self.worker_modules:

            # load the worker module and create the worker object
            module = importlib.import_module('.workers.'+worker_module, __package__)
            self.logger.debug("Imported %s", module)
            worker = module.WorkerThread(self)

            # keep a copy for use later
            self.workers.append(worker)

            # hook up on_tick callback if there is one (more importantly; don't if there isn't)
            if 'on_tick' in dir(worker):
                self.worker_ticks.append(getattr(worker, 'on_tick'))

            # start the thread running in the background - set the daemon flag so that we do not
            #  have to worry about killing it when the time comes to exit
            thread = threading.Thread(name='Worker ' + str(len(self.workers)), target=worker.main)
            thread.setDaemon(True)
            thread.start()

        # Start the server interface communications message loop on its own thread
        self.comms_thread = threading.Thread(name='Comms', target=self.server_interface.msg_loop)
        self.comms_thread.setDaemon(True)
        self.comms_thread.start()

        # Run the main thread loop on this, the main thread
        #  Note that when this thread exits all the background threads will die automatically
        self.main()

    def main(self):
        """ Main thread application loop
        :return:
        """

        last_warning = round(time.time(), 0)

        while self.comms_thread.is_alive():

            now = round(time.time(), 0)

            desired_states = []

            for worker in self.workers:
                # TODO - cache these methods
                if 'get_desired_status' in dir(worker):
                    result = getattr(worker, 'get_desired_status')()
                    if isinstance(result, DesiredStatus):
                        desired_states.append(result)

            # TODO create aggregate desired result - using layers, priorities, weights, utility, etc, whatever ...
            if len(desired_states) > 0:
                desired_status = desired_states.pop()

                if self.desired_status is None:
                    self.logger.info('got the first desired status')
                    # DEBUG ONLY CODE - BEGIN
                    for attr_name in desired_status.attributes:
                        self.logger.debug("%s = %s", attr_name, getattr(desired_status, attr_name))
                    # DEBUG ONLY CODE - END

                if self.desired_status is None or not desired_status.is_same_as(self.desired_status):
                    #self.logger.debug('new desired status - locking')

                    got_lock = self.tick_lock.acquire(False)
                    if not got_lock:
                        self.on_tick_done.wait()
                        got_lock = self.tick_lock.acquire(False)

                    if not got_lock:
                        self.logger.error('unable to obtain tick_lock in main loop - skipping')

                    if got_lock:
                        #self.logger.debug('new desired status - updating')
                        self.desired_status = desired_status
                        self.tick_lock.release()
                        #self.logger.debug('new desired status - unlocked')

            else:
                if last_warning < now:
                    last_warning = now
                    if self.desired_status is None:
                        self.logger.warning('no desired status and no desired states from the workers')
                    else:
                        self.logger.warning('no desired states from the workers')


            # don't hog the CPU - is it a good idea to give everything else a chance to breathe
            time.sleep(0.01)

        # Exiting bot ...
        self.logger.info('bot exiting at tick %s', self.tick)
        for worker in self.workers:
            if 'on_exit' in dir(worker):
                getattr(worker, 'on_exit')()

    def on_tick(self, number):
        """ callback which is called when the tick value has changed - while the race is running
            this will be every message. This method is responsible for:
            1/ triggering the callback to all the background threads to let them know that
                a server tick has occurred
            2/ causing some command to be sent to the server
        :param number:
        :return:
        """

        got_lock = True
        started_waiting = time.time()
        while not self.tick_lock.acquire(False):
            # unable to obtain lock
            now = time.time()
            if now - started_waiting > 0.5:
                # been waiting too long
                self.logger.error('timeout while attempting to obtain tick_lock in callback - skipping')
                got_lock = False
                break
        try:
            if got_lock:
                self.on_tick_done.clear()
                self.tick = number
                self.event_publish('tick')

                #
                #  This is synchronous and will run on the comms thread
                #   It should only be used for things which need to block - e.g. don't use unless you have to
                #
                for method in self.worker_ticks:
                    method(number)

                # Push desired result data to server_interface
                if not self.desired_status.lane is None:
                    self.bot_state.target_lane = self.desired_status.lane
                if not self.desired_status.speed is None:
                    self.bot_state.target_speed = self.desired_status.speed

        finally:
            self.tick_lock.release()

        self.on_tick_done.set()


    def on_all_messages(self, message_type):
        """ callback which is called when any message is received
        :param message_type:
        :return:
        """
        self.event_publish(message_type)

    def event_publish(self, event_name):
        """ Signal to the other threads that the event with the given name has just occurred
        :param event_name:
        :return:
        """
        if event_name in self.events_subscriptions:
            for subscription in self.events_subscriptions[event_name]:
                subscription.set()

    def event_subscribe(self, event_name):
        """ Obtain an object which will indicate (across thread boundaries) that
             the event with the given name has just occurred
        :param event_name:
        :return:
        """
        event = threading.Event()
        if not event_name in self.events_subscriptions:
            self.events_subscriptions[event_name] = []
        self.events_subscriptions[event_name].append(event)
        return event

    def pp(self, data):
        import pprint
        pp = pprint.PrettyPrinter(indent=4)
        self.logger.debug(pp.pformat(data))
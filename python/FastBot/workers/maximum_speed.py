import time
import math
from ..data import DesiredStatus


class WorkerThread:

    def __init__(self, bot):
        """ Init the worker thread
        :return;
        """

        self.bot = bot
        self.name = __name__

        self.upper_speed_limit = 10.0                           # Desired final value for this is 10.0 (float)
        self.minimum_speed = 1.0                                # Used to avoid bugs which result in no speed

        self.maximum_grip = 0.4                                 # this is an 'educated assumption'
        self.maximum_grip_history = []
        self.maximum_grip_history.append(self.maximum_grip)

        self.number_segments = 5                                # This number has to be odd, 5 is good, 7 may be better

        self.precision = 3                                      # prevent floating point rounding errors
        self.fix_distance_precision = 6                         # prevent floating point rounding errors

        self.bot_state = self.bot.bot_state
        self.race_status = self.bot.race_status

        self.done_whole_track = False
        self.last_current_speed = {'lap': 0, 'piece_index': 0, 'distance': 0.0, 'crashed': False}

        import logging

        self.logger = logging.getLogger(self.name)

        self.logger.debug('%s init done', self.name)

    def main(self):
        """ Main loop for this worker thread
        :return:
        """

        self.logger.info('thread started')

        while True:

            if not 'pieces' in self.race_status.track:
                # No track data yet
                time.sleep(0.01)
                continue

            if not self.done_whole_track:
                self.calculate_for_track()

            elif len(self.bot_state.position) > 0:
                # we know our current position
                index = self.bot_state.piece

                if index >= len(self.race_status.track['pieces']) - 1:
                    self.calculate_for_piece(0)
                else:
                    self.calculate_for_piece(index + 1)

                self.calculate_for_piece(index)

            self.record_current_status()

            # don't hog the CPU - is it a good idea to give everything else a chance to breathe
            time.sleep(0.01)

        self.logger.info('thread exiting')

    def record_current_status(self):
        if not 'pieces' in self.race_status.track:
            # No track data yet
            return False

        tick = self.bot.tick
        speed = self.bot_state.speed
        lap = self.bot_state.lap
        lane_index = self.bot_state.lane
        piece_index = self.bot_state.piece
        distance = self.bot_state.position['piecePosition']['inPieceDistance']
        angle = self.bot_state.angle

        if self.bot_state.crashed > self.bot_state.spawned:
            crashed = True
        else:
            crashed = False

        if self.last_current_speed['lap'] == lap:
            if self.last_current_speed['piece_index'] == piece_index:
                if self.last_current_speed['distance'] == distance:
                    if self.last_current_speed['crashed'] == crashed:
                        # Don't record the same data twice!
                        return False

        self.last_current_speed = {'lap': lap, 'piece_index': piece_index, 'distance': distance, 'crashed': crashed, 'angle': angle}

        piece = self.race_status.track['pieces'][piece_index]
        if 'lanes' in piece:

            lane = piece['lanes'][lane_index]

            if not 'history' in lane:
                lane['history'] = []

            record = {
                'lap': lap,
                'distance': distance,
                'speed': speed,
                'crashed': crashed,
                'angle': angle,
                'tick': tick
            }

            if 'radius' in lane and lane['radius'] > 0:
                record['grip'] = self.calculate_current_grip(lane['radius'], speed)

            lane['history'].append(record)

        return True

    def calculate_for_track(self):

        if not 'pieces' in self.race_status.track:
            # No track data yet
            return False

        self.logger.info('calculating maximum speed for entire track')

        for index in xrange(0, len(self.race_status.track['pieces'])):
            if not self.calculate_for_piece(index):
                return False

        self.done_whole_track = True

    def calculate_for_piece(self, index):

        if not 'pieces' in self.race_status.track:
            # No track data yet
            return False

        # We always want to go as fast as the car, track and circumstances will let us.

        #
        # various factors which affect the maximum speed
        # - maximum grip/drift
        # - we're given length and width of the car, plus the position of the guide flag
        # so, we can detect whether we're rear or front 'wheel' driven
        # - is the track a corner piece?
        # - is there a corner piece coming up?
        #

        piece = self.race_status.track['pieces'][index]

        if index >= len(self.race_status.track['pieces']) - 1:
            next_piece = self.race_status.track['pieces'][0]
        else:
            next_piece = self.race_status.track['pieces'][index + 1]

        if index <= 0:
            last_piece = self.race_status.track['pieces'][len(self.race_status.track['pieces']) - 1]
        else:
            last_piece = self.race_status.track['pieces'][index - 1]

        if not 'type' in piece or not 'type' in next_piece or not 'type' in last_piece:
            # need to wait for data to be fleshed out before I can use it
            return False

        for lane in piece['lanes']:
            next_lane = next_piece['lanes'][lane['index']]
            last_lane = last_piece['lanes'][lane['index']]

            # Attempt to learn from crashes
            if 'learned_maximum_speed' in lane:
                speed_limit = lane['learned_maximum_speed']

            elif 'maximum_speed_hint' in lane:
                speed_limit = lane['maximum_speed_hint']

            else:
                speed_limit = self.upper_speed_limit

            # --------------------------------------------------------------------------------------
            # Learning ...

            #TODO: Have this include angle data!

            last_crash = self.find_last_crash(piece, lane)
            if isinstance(last_crash, dict):
                # we have crashed previously, so let's learn from that

                # firstly, let's stay below the speed we crashed at by a magic number
                speed_limit = last_crash['speed'] * 0.95
                if 'maximum_speed' in lane and speed_limit > lane['maximum_speed']:
                    speed_limit = lane['maximum_speed']
                if speed_limit > self.upper_speed_limit:
                    speed_limit = self.upper_speed_limit
                if speed_limit < self.minimum_speed:
                    speed_limit = self.minimum_speed
                if not 'learned_maximum_speed' in lane or lane['learned_maximum_speed'] > speed_limit:
                    lane['learned_maximum_speed'] = speed_limit
                    if 'grip' in last_crash:
                        self.logger.info(
                            'learned speed {0} is too fast for piece {1} lane {2} due to crashing - new limit {3} (grip was {4})'.format(
                                last_crash['speed'], piece['index'], lane['index'], speed_limit, last_crash['grip']))
                    else:
                        self.logger.info(
                            'learned speed {0} is too fast for piece {1} lane {2} due to crashing - new limit {3} (no grip info)'.format(
                                last_crash['speed'], piece['index'], lane['index'], speed_limit))

                # secondly, let's stay below the grip used by a magic number
                #TODO: Make this more intelligent (Greater angle's means greater reduction in grip)
                if 'grip' in last_crash:
                    if last_crash['grip'] <= self.maximum_grip:
                        new_maximum_grip = last_crash['grip'] * 0.95
                        self.logger.debug(
                            'learned grip {0} is too high for piece {1} lane {2} due to crashing - reducing {3} to {4}'.format(
                                last_crash['grip'], piece['index'], lane['index'], self.maximum_grip, new_maximum_grip))
                        self.maximum_grip = new_maximum_grip
                        self.maximum_grip_history.append(self.maximum_grip)

            # --------------------------------------------------------------------------------------
            # Calculating ...

            #TODO: Detect huge increases in angle and completely pull off the gas
            maximum_speeds = []
            highest_maximum_speed = 0.0
            for segment in xrange(0, self.number_segments):
                if piece['type'] == 'straight':
                    maximum_speed = self.calculate_for_lane_in_straight_piece(piece, next_piece, last_piece, lane,
                                                                              next_lane, last_lane, segment,
                                                                              speed_limit)
                elif piece['type'] == 'corner':
                    maximum_speed = self.calculate_for_lane_in_corner_piece(piece, next_piece, last_piece, lane,
                                                                            next_lane, last_lane, segment, speed_limit)
                maximum_speed = round(maximum_speed, self.precision)

                if maximum_speed < self.minimum_speed:
                    maximum_speed = self.minimum_speed

                maximum_speeds.append(maximum_speed)
                if maximum_speed > highest_maximum_speed:
                    highest_maximum_speed = maximum_speed

            if highest_maximum_speed > self.upper_speed_limit:
                highest_maximum_speed = self.upper_speed_limit

            lane['maximum_speeds'] = maximum_speeds
            lane['maximum_speed'] = highest_maximum_speed

            lane['maximum_speed'] = float(lane['maximum_speed'])

        return True

    def get_maximum(self, piece, lane, last):
        """
        reads the maximum speed value from the object, but if it's not there
        will try and crudely work something out - this is to be used for linking
        values together from pieces during calculations.

        :param piece:
        :param lane:
        :param last:
        :return:
        """

        next_maximum_speed = self.upper_speed_limit

        if 'maximum_speeds' in lane:
            if last:
                next_maximum_speed = lane['maximum_speeds'][self.number_segments - 1]
            else:
                next_maximum_speed = lane['maximum_speeds'][0]

        elif 'maximum_speed' in lane:
            next_maximum_speed = lane['maximum_speed']

        elif 'maximum_speed' in piece:
            next_maximum_speed = piece['maximum_speed']

        else:
            # Do something crude here - the number will be refined later

            if piece['type'] == 'straight':
                next_maximum_speed = self.upper_speed_limit

            elif piece['type'] == 'corner':
                if 'radius' in lane:
                    radius = lane['radius']
                else:
                    radius = piece['radius']
                next_maximum_speed = self.calculate_for_corner(radius, self.maximum_grip)

        return float(next_maximum_speed)

    def calculate_for_lane_in_straight_piece(self, piece, next_piece, last_piece, lane, next_lane, last_lane, segment,
                                             speed_limit):
        maximum_speed = float(speed_limit)
        next_maximum_speed = self.get_maximum(next_piece, next_lane, False)
        result = self.calculate_point_on_gradient(maximum_speed, next_maximum_speed, self.number_segments, segment)
        if result > speed_limit:
            result = speed_limit
        return result

    def calculate_for_lane_in_corner_piece(self, piece, next_piece, last_piece, lane, next_lane, last_lane, segment,
                                           speed_limit):
        if piece['index'] == 15:
            pass

        if 'radius' in lane:
            radius = lane['radius']
        else:
            radius = piece['radius']

        maximum_speed = self.calculate_for_corner(radius, self.maximum_grip)
        if maximum_speed > speed_limit:
            maximum_speed = speed_limit

        half_segments = (self.number_segments / 2)

        if segment < half_segments:
            # before the apex
            last_maximum_speed = self.get_maximum(last_piece, last_lane, False)
            result = self.calculate_point_on_gradient(last_maximum_speed, maximum_speed, half_segments, segment)

        elif segment == half_segments:
            # on the apex
            result = maximum_speed

        else:
            # after the apex - on the exit
            next_maximum_speed = self.get_maximum(next_piece, next_lane, False)
            result = self.calculate_point_on_gradient(maximum_speed, next_maximum_speed, half_segments, segment)

        if result > speed_limit:
            result = speed_limit
        return result

    def calculate_point_on_gradient(self, start, target, number_segments, segment):
        number_segments = float(number_segments)
        start = float(start)
        target = float(target)
        difference = start - target
        dps = difference / number_segments
        m = (number_segments - 1) - segment
        return target + (dps * m)

    def calculate_current_grip(self, radius, speed):
        v = speed * speed
        current_grip = v / radius
        return current_grip

    def calculate_for_corner(self, radius, grip):
        # mv2 / r <= maxG

        # Do I know the radius?
        # Yes

        # Do I know my velocity?
        # Technically no, but I have speed which is probably good enough

        # Do I know my own mass?
        # No - is it relevant in this simulation? how?

        # Do I know the limit of my own grip force?
        # No - learn it over time? would have to crash to find the limit
        #        could this be learned by watching other cars on the track? too hard?

        # So, this is probably the best I can do ...
        return math.sqrt(grip * radius)

    def find_last_crash(self, piece, lane):
        if not 'history' in lane:
            return False

        result = {}

        found_crash = False
        last_crash_speed = 0.0

        num_history = len(lane['history'])
        for index in xrange(num_history - 1, 0, -1):  # read backwards because we only want the last
            record = lane['history'][index]

            if record['crashed']:
                # Found a record where we have crashed!
                found_crash = True

            else:
                if found_crash:
                    # this is the record just before the crash, so this is the speed info we want
                    result['speed'] = float(record['speed'])
                    result['grip'] = float(record['grip'])
                    result['angle'] = float(record['angle'])
                    return result

        return False

    def get_desired_status(self):
        if not 'pieces' in self.race_status.track:
            # No track data yet
            return False

        lane_index = self.bot_state.lane
        piece_index = self.bot_state.piece

        piece = self.race_status.track['pieces'][piece_index]
        if 'lanes' in piece:

            current_lane_maximum_speed = 0.0

            best_lane_maximum_speed = 0.0
            best_lane_index = lane_index

            for lane in piece['lanes']:
                if 'maximum_speed' in lane:
                    if lane['index'] == lane_index:
                        current_lane_maximum_speed = lane['maximum_speed']
                    if float(lane['maximum_speed']) > (float(best_lane_maximum_speed) * 1.1):
                        best_lane_maximum_speed = lane['maximum_speed']
                        best_lane_index = lane['index']
                else:
                    # Not enough data
                    return None

                if lane['index'] == lane_index and 'maximum_speeds' in lane:
                    distance = self.bot_state.position['piecePosition']['inPieceDistance']
                    if round(distance, self.fix_distance_precision) > round(lane['length'], self.fix_distance_precision):
                        self.logger.warn(
                            'distance through piece {0} exceeds length of {1}'.format(round(distance, self.fix_distance_precision), round(lane['length'], self.fix_distance_precision)))
                        lane['length'] = round(distance, self.fix_distance_precision)
                    else:
                        segment = int((float(distance) / float(lane['length'])) * int(self.number_segments))
                        if segment >= self.number_segments:
                            segment = self.number_segments - 1
                        current_lane_maximum_speed = lane['maximum_speeds'][segment]

            if current_lane_maximum_speed < self.minimum_speed:
                current_lane_maximum_speed = self.minimum_speed

            status = DesiredStatus(self.bot)
            status.speed = float(current_lane_maximum_speed)
            status.lane = best_lane_index
            return status

        # Not enough data
        return None

    def pp(self, data):
        import pprint

        pp = pprint.PrettyPrinter(indent=4)
        self.logger.debug(pp.pformat(data))

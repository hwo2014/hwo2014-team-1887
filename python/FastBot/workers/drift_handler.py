from ..data import DesiredStatus


class WorkerThread:
    def __init__(self, bot):
        """ Init the worker thread
        :return;
        """

        #TODO: Generate 3 decent starting magic numbers dependant on track radius
        #TODO: Learning module that generates new magic numbers dependant on existing library
        #TODO: Re-learn recommendations based on historical data after a crash

        self.bot = bot
        self.bot_state = self.bot.bot_state
        self.name = __name__

        self.latest_diffs = []
        self.latest_angles = []
        self.start_angle = 0.0

        self.emergency_brake = 30
        self.interest_differential = 1

        self.should_emergency_brake = False

        import logging
        self.logger = logging.getLogger(self.name)

        self.logger.debug('%s init done', self.name)

    def main(self):

        tick_event = self.bot.event_subscribe("tick")

        while True:
            if self.bot_state.angle > 0.00:
                if self.start_angle > 0.00:
                    diff_array = [self.start_angle, abs(self.bot_state.angle)]
                    if diff_array[1] > diff_array[0] and abs(self.bot_state.angle) > 5:
                        for a, b in zip(diff_array[::1], diff_array[1::1]):
                            self.latest_diffs.insert(0, 100 * (b - a) / a)
                            break
                        self.latest_angles.insert(0, self.bot_state.angle)
                        if len(self.latest_diffs) >= 5:
                            self.latest_diffs = self.latest_diffs[0:5]
                        if len(self.latest_angles) >= 5:
                            self.latest_angles = self.latest_angles[0:5]
                    else:
                        self.latest_diffs = []
                        self.latest_angles = []
                        self.start_angle = 0.00

                self.start_angle = abs(self.bot_state.angle)
            else:
                #Reset ready for the next period of fail
                self.start_angle = 0.00
                self.latest_diffs = []
                self.latest_angles = []

            if len(self.latest_diffs) > 4:
                avg = sum(self.latest_diffs) / len(self.latest_diffs)

                if avg > self.interest_differential and self.latest_angles[0] > self.emergency_brake:
                    #This is something of interest, now check the angle itself
                    if not self.should_emergency_brake:
                        self.logger.info("Drift Handler - Emergency Brake Start")
                        self.should_emergency_brake = True
                else:
                    if self.should_emergency_brake:
                        self.logger.info("Drift Handler - Emergency Brake Stop")
                    self.should_emergency_brake = False
            else:
                if self.should_emergency_brake:
                    self.logger.info("Drift Handler - Emergency Brake Stop")
                self.should_emergency_brake = False

            tick_event.wait()
            tick_event.clear()

        self.logger.info('Drift Handler - Destroyed')

    def get_desired_status(self):
        if self.should_emergency_brake == True:
            status = DesiredStatus(self.bot)
            status.speed = 0.0

            return status
        else:
            return None
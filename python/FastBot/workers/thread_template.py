
import time

class WorkerThread:

    def __init__(self, bot):
        """ Init the worker thread
        :return;
        """
        self.bot = bot
        self.name = __name__

        import logging
        self.logger = logging.getLogger(self.name)

        self.logger.debug('%s init done', self.name)


    def main(self):
        """ Main loop for this worker thread
        :return:
        """

        self.logger.info('thread started')

        while True:
            # TODO implement something here - this is the bit which runs in the background
            time.sleep(1)
            self.logger.debug('thread is still alive')
            pass

        self.logger.info('thread exiting')

    def on_tick(self, number):
        """ Event handler called for each tick
        :param number:
        :return:
        """

        self.logger.debug(self.name + ' on tick called')

        # TODO maybe implement something here - called every tick, probably a good time to sync with the rest of the bot
        pass



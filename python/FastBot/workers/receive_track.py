"""
This is the receive track worker module.

This module is responsible for:
1/ waiting for track layout data to be available
2/ putting the track layout data in to the race status data storage
3/ Any pre-calculation that doesn't fit within any of the other workers
4/ Loading/saving of any persistent data we may have about a track
    - yes, we don't have persistent storage when running in the cloud,
            but we do when running locally and we can take whatever we
            commit with us to the cloud
"""

import math
import pickle
import os.path
import time

class WorkerThread:

    def __init__(self, bot):
        self.bot = bot
        self.race_status = self.bot.race_status
        self.server_interface = self.bot.server_interface

        self.save_persistence_data = True
        self.load_persistence_data = False

        self.saved = False
        self.ready = False

        self.saving = False

        import logging
        self.logger = logging.getLogger(__name__)

        self.logger.debug('%s init done', __name__)

    def main(self):
        self.logger.debug('thread started')
        self.logger.info('waiting for race track data')

        event_game_init = self.bot.event_subscribe('game_init')
        event_game_init.wait()    # block the thread while waiting for the game_init event to be fired

        event_game_end = self.bot.event_subscribe('game_end')

        # grab a copy of the raw data
        data = self.server_interface.race

        self.logger.info('race track data received')
        track_data = self.process_track_data(data['track'])

        self.race_status.track = track_data
        self.race_status.cars = data['cars']
        self.race_status.session = data['raceSession']

        self.logger.info('now ready')
        self.ready = True

        while True:

            # wait for the game to end
            event_game_end.wait()
            event_game_end.clear()

            self.logger.info('no longer ready - now resetting track state')
            self.ready = False

            # save the track state from the end of the current game
            self.save_track_state()                                         # TODO does this slow us down much?

            # re-process track data so that
            #  - we remove the historical information for the last game
            #  - but keep all the learning information
            track_data = self.race_status.track
            self.race_status.track = {}                             # hide the data from other threads

            track_data = self.process_track_data(data['track'])
            self.race_status.track = track_data                     # make the data available again

            self.logger.info('ready again')
            self.ready = True

    def process_track_data(self, track_data):

        if self.load_persistence_data:
            loaded_data = self.load_track_state(track_data['id'])
            if not loaded_data is None:
                track_data = loaded_data

        for index in xrange(0, len(track_data['pieces'])):
            track_data['pieces'][index]['index'] = index

        for piece in track_data['pieces']:

            # add lane data for each piece
            if not 'lanes' in piece:

                piece['lanes'] = []
                for index in xrange(0, len(track_data['lanes'])):
                    piece['lanes'].append({})

                for lane in track_data['lanes']:
                    piece['lanes'][int(lane['index'])] = {
                        'index': lane['index'],
                        'offset': lane['distanceFromCenter']
                    }

            # remove previous race history if it's already there
            for lane in piece['lanes']:
                if 'history' in lane:
                    lane['history'] = []

            if not 'switch' in piece:
                piece['switch'] = False

            if (not 'type' in piece and 'angle' in piece) or ('type' in piece and piece['type'] == 'corner'):
                piece['type'] = 'corner'

                for lane in piece['lanes']:

                    # radius is distance from center of the circle
                    # lane['offset'] is the difference in the radius for this lane
                    if not 'angle' in lane:
                        lane['angle'] = piece['angle']
                    if not 'radius' in lane:
                        lane['radius'] = float(piece['radius']) + float(lane['offset'])
                    if not 'length' in lane:
                        circumference = 2 * math.pi * lane['radius']
                        lane['length'] = circumference * (lane['angle'] / 360.0)
                        if lane['length'] < 0:
                            lane['length'] = lane['length'] * -1

            else:
                piece['type'] = 'straight'
                piece['angle'] = 0.0
                piece['radius'] = 0

                for lane in piece['lanes']:
                    if not 'angle' in lane:
                        lane['angle'] = piece['angle']
                    if not 'radius' in lane:
                        lane['radius'] = 0
                    if not 'length' in lane:
                        lane['length'] = piece['length']

        return track_data

    def on_exit(self):
        time.sleep(1)
        while self.saving:
            time.sleep(1)

    def save_track_state(self):
        if self.save_persistence_data and 'id' in self.race_status.track:
            self.saving = True

            data = self.race_status.track
            track_id = self.race_status.track['id']

            self.logger.info('saving track data for %s', track_id)
            pkl_file = open(track_id + '_track.pkl', 'wb')
            pickle.dump(data, pkl_file)
            pkl_file.close()

            game_id = self.bot.bot_state.game_id
            if not game_id is None and game_id != '':
                self.logger.info('stashing race data for %s %s', track_id, game_id)
                pkl_file = open(track_id + '_' + game_id + '_track.pkl', 'wb')
                pickle.dump(data, pkl_file)
                pkl_file.close()

            self.saving = False
            return True
        else:
            return False

    def load_track_state(self, track_id):
        if self.load_persistence_data:
            if os.path.isfile(track_id + '_track.pkl'):
                self.logger.info('loading stored track data for %s', track_id)
                pkl_file = open(track_id + '_track.pkl', 'rb')
                track_data = pickle.load(pkl_file)
                pkl_file.close()
                return track_data
        else:
            return False

    def pp(self, data):
        import pprint
        pp = pprint.PrettyPrinter(indent=4)
        self.logger.debug(pp.pformat(data))




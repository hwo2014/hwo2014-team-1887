
class WorkerThread:

    def __init__(self, bot):
        self.bot = bot

        self.bot_state = self.bot.bot_state
        self.competitor_states = self.bot.competitor_states

        import logging
        self.logger = logging.getLogger(__name__)

        self.logger.debug('%s init done', __name__)

    def main(self):

        tick_event = self.bot.event_subscribe("tick")

        # TODO
        #  cars in front  - overtake, or ram
        #  cars behind    - ignore (slow speeds), or evade (high speeds)
        #  cars beside    - ignore - indicate don't change lane somehow

        last_seen = []

        while True:

            seen = []
            infront = []
            behind = []

            for competitor in self.competitor_states:

                if competitor.peice == self.bot_state.peice + 1:
                    infront.append(competitor)
                    seen.append(competitor)

                if competitor.peice == self.bot_state.peice - 1:
                    behind.append(competitor)
                    seen.append(competitor)

                if competitor.peice == self.bot_state.peice:

                    my_distance = self.bot_state.position['piecePosition']['inPieceDistance']
                    their_distance = competitor.position['piecePosition']['inPieceDistance']

                    if my_distance > their_distance:
                        behind.append(competitor)
                        seen.append(competitor)
                    else:
                        infront.append(competitor)
                        seen.append(competitor)

                if competitor in seen and not competitor in last_seen:
                    if competitor in infront:
                        self.logger.info("Competitor %s %s is close by in front of me".format(competitor.name, competitor.color))
                    else:
                        self.logger.info("Competitor %s %s is close by behind me".format(competitor.name, competitor.color))

            last_seen = seen

            tick_event.wait()
            tick_event.clear()

        self.logger.info("Thread exited")

    def get_desired_status(self):

        return None


import json
import socket
import sys

import time
import pprint

class Bot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.tick = 0

        self.color = 'None'
        self.game_init = False
        self.floored = False
        self.started = time.time()
        self.race = {}
        self.crashed = 0

        self.piece = 0
        self.piece_data = {}
        self.distances = []
        self.speed = 0
        self.last_angle = 0

    def pp(self, data):
        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(data)

    def msg(self, msg_type, data):
        message = json.dumps({"msgType": msg_type, "data": data})
        print "Sending:", message
        self.send(message)

    def send(self, msg):
        self.socket.send(msg + "\n")

    def createRace(self, track, password, cars):
        return self.msg("createRace",
                        {
                            "botId": {
                                "name": self.name,
                                "key": self.key
                            },
                            "trackName": track,     # Keimola
                            "password": password,
                            "carCount": cars
                        }
        )

    def joinRace(self, track, password, cars):
        return self.msg("joinRace",
                        {
                            "botId": {
                                "name": self.name,
                                "key": self.key
                            },
                            "trackName": track,     # Keimola
                            "password": password,
                            "carCount": cars
                        }
        )

    def switchLane(self, direction):
        return self.msg("switchLane", direction)

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        if throttle > 1:
            throttle = 1
        if throttle < 0:
            throttle = 0
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def advanced_run(self, start_type, track, password, cars):
        if start_type != 'join':
            self.createRace(track, password, cars)
        else:
            self.joinRace(track, password, cars)
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_your_car(self, data):
        self.color = data['color']
        print "Received my car data; I am", self.color
        self.ping()

    def on_game_init(self, data):
        print("Game init")
        self.pp(data)
        self.race = data['race']
        self.game_init = True

    def on_game_start(self, data):
        print("Race started")

    def on_car_positions(self, data):

        self.throttle(0.65)
        return

        if self.crashed > 0 and self.tick > self.crashed:
            print "There was a crash at tick", self.crashed, "it is now", self.tick
            if self.tick - self.crashed > 10:
                quit()
            self.ping()
            return

        for position in data:
            if position['id']['color'] == self.color:
                my_position = position

        angle = my_position['angle']
        piece, distance = my_position['piecePosition']['pieceIndex'], my_position['piecePosition']['inPieceDistance']


        if piece != self.piece:
            self.piece = piece
            self.distances = []
            self.piece_data = self.race['track']['pieces'][piece]
            self.pp(self.piece_data)
        else:
            if len(self.distances) > 0:
                last_distance = self.distances.pop()
                self.speed = distance - last_distance
                self.distances.append(last_distance)
            self.distances.append(distance)

        #if 'switch' in self.piece_data:
        #    self.switchLane("Right")
        #    print "Switched"
        #    return

        print "My position is: piece={0}, distance={1}, angle={2}, speed={3} @ tick={4}".format(str(piece).ljust(3), str(distance).ljust(16), str(angle).ljust(20), str(self.speed).ljust(15), self.tick),

        if angle == 0.0:
            rational = 0.0
        elif angle < 0.0:
            rational = angle * -1.0
        else:
            rational = angle

        angle_change = rational - self.last_angle
        self.last_angle = rational

        if angle_change > 0:
            # angle is increasing - we're probably entering a corner, so slow down
            print " <<<-- ",

            if rational < 0.1:
                throttle = 0.7

            elif rational < 0.3:
                throttle = 0.65

            elif rational < 0.5:
                throttle = 0.6

            elif rational < 1:
                throttle = 0.5

            elif rational < 5:
                throttle = 0.3

            elif rational < 10:
                throttle = 0.1

            else:
                throttle = 0.0

        else:
            # angle is decreasing - we're probably leaving a corner, so speed up
            print " -->>> ",

            if rational < 0.2:
                throttle = 1

            elif rational < 0.3:
                throttle = 0.9

            elif rational < 0.6:
                throttle = 0.8

            elif rational < 2:
                throttle = 0.7

            elif rational < 5:
                throttle = 0.65

            elif rational < 10:
                throttle = 0.6

            else:
                throttle = 0.55


        # for this specific track, the maximum cornering speed is 6.5
        if 'angle' in self.piece_data:

            if self.speed < 6:
                throttle = throttle + 0.1

            elif self.speed > 6.5:
                throttle = 0.0

            elif self.speed >= 6:
                throttle = 0.1


        #throttle = 0.55 + ((1.0 / 2000) * (self.tick / 10))

        if throttle > 1:
            throttle = 1
        if throttle < 0:
            throttle = 0

        print " - throttle", throttle
        self.throttle(throttle)

        return

        if self.tick > 700 and self.tick < 800:
            self.floored = False
            self.throttle(0.0)

        elif self.floored:
            self.ping()

        else:
            now = time.time()
            duration = now - self.started
            max_duration = 20
            target = 0.65
            if duration < max_duration:
                self.throttle( (target / max_duration) * duration )
            else:
                self.throttle(target)
                self.floored = True

    def on_crash(self, data):
        print "Someone crashed at tick", self.tick
        self.crashed = self.tick
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'yourCar': self.on_your_car,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            print "Received:", line.strip()
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if 'gameTick' in msg:
                self.tick = msg['gameTick']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print "\n\n"
                print("Got Unknown Message {0}".format(msg_type))
                self.pp(data)
                print "\n\n"
                self.ping()
            line = socket_file.readline()




import pickle
import pprint
import os
import sys
import csv

def pp(data):
    pp = pprint.PrettyPrinter(indent=4)
    print(pp.pformat(data))

if len(sys.argv) == 2:
    file = sys.argv[1]
    interactive = False

else:
    files = []
    for file in os.listdir("."):
        if file.endswith(".pkl"):
            files.append(file)
    print ""
    print "Available files:"
    print ""
    for n in xrange(0, len(files)):
        print str(n).rjust(5), '-', files[n]
    print ""
    index = raw_input("Which file? ")
    if index == '':
        exit()
    file = files[int(index)]
    interactive = True

#######################################################################################################################
# Read and output the file

print "Showing max speeds from file", file
print "-" * 85

#pp = pprint.PrettyPrinter(indent=4)

pkl_file = open(file, 'rb')
contents = pickle.load(pkl_file)
pkl_file.close()

target = open(file + '.csv', 'wb')
log = csv.writer(target)
log.writerow(['Index', 'Segment', 'Type', 'lane0', 'lane1', 'lane2', 'lane3'])

num_segments = len(contents['pieces'][0]['lanes'][0]['maximum_speeds'])
segment = 0

print 'index'.rjust(5), '|', 'segment'.rjust(10), '|', 'type'.rjust(10), '|',
print 'lane0'.rjust(10), '|', 'lane1'.rjust(10), '|', 'lane2'.rjust(10), '|', 'lane3'.rjust(10)
print "-" * 85

for piece in contents['pieces']:
    for segment in xrange(0, num_segments):

        row = []

        if not 'index' in piece:
            pp(piece)
            piece['index'] = '?'

        print str(piece['index']).rjust(5), '|', str(segment).rjust(10), '|', str(piece['type']).rjust(10), '|',
        row.append(piece['index'])
        row.append(segment)
        row.append(piece['type'])

        for lane in piece['lanes']:
            maximum_speed = lane['maximum_speeds'][int(segment)]
            print str(maximum_speed).rjust(10), '|',
            row.append(maximum_speed)

        print

        log.writerow(row)


print "-" * 85
print "written", file + ".csv"

if interactive:
    raw_input("Press enter to close")
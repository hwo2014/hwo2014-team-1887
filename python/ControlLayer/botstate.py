class botState(object):
    def __init__(self, bot, name, key, color):

        self.bot = bot

        # init logging
        import logging
        self.logger = logging.getLogger(__name__)

        # -------------------------------------------------------------
        # data which is consistent throughout the race

        self.name = name
        self.key = key
        self.color = color
        self.game_id = ''

        self.dimensions = {'length': 0.0, 'width': 0.0, 'guideFlagPosition': 0.0}
        self.length = 0.0
        self.width = 0.0
        self.guide_flag = 0.0

        # -------------------------------------------------------------
        # data which is constantly changing

        self.position = {'piecePosition': {'pieceIndex': 0, 'inPieceDistance': 0.0}}
        self.last_position = self.position
        self.crashed = 0                        # game tick of the last crash, 0 = not crashed this race
        self.spawned = 0                        # game tick of the last spawn, 0 = start of race
        self.lap = 0                            # number of laps completed by this car, so far
        self.piece = 0                          # index of the piece we currently occupy
        self.lane = 0                           # index of the lane we currently occupy
        self.angle = 0.0                        # current angle of the car relative to the track (negative = left)
        self.rational_angle = 0.0               # current angle but always a positive value
        self.speed = 0.0                        # distance traveled since the last update (usually)
        self.turbo_available = False
        self.turbo = None

        # TODO add lapFinished information to here

        # -------------------------------------------------------------
        # desired state

        self.use_turbo = False                  # 1: set this to indicate that we should activate turbo
        self.target_lane = 0                    # 2: set this to indicate which lane we want
        self.target_speed = 0.0                 # 3: set this to indicate the speed we want

        # -------------------------------------------------------------
        # other

        self.last_throttle = 0.0
        self.last_speed = 0.0
        self.pending_switch_to_lane = 0

    def update_dimensions(self, data):
        self.dimensions = data['dimensions']
        self.length = self.dimensions['length']
        self.width = self.dimensions['width']
        self.guide_flag = self.dimensions['guideFlagPosition']

    def update_position(self, position):

        self.last_position = self.position
        self.position = position

        self.angle = self.position['angle']
        if self.angle == 0.0:
            self.rational_angle = 0.0
        elif self.angle < 0.0:
            self.rational_angle = self.angle * -1.0
        else:
            self.rational_angle = self.angle

        self.lane = self.position['piecePosition']['lane']['endLaneIndex']
        self.lap = self.position['piecePosition']['lap']

        last_piece_index = self.last_position['piecePosition']['pieceIndex']
        new_piece_index = self.position['piecePosition']['pieceIndex']
        if last_piece_index != new_piece_index:
            self.piece = new_piece_index

            # TODO - correct speed calculation for when we move track piece
            # NB. to correctly calculate speed, we need to know the length of the last piece of track
            #      so that we know how much of it was travelled before we were on this piece

            if self.key != '':
                self.logger.info('lap {0} piece {1} speed {2}'.format(self.lap, self.piece, self.speed))

        else:
            # because we can't calculate speed correctly when moving from one piece to another instead
            # we'll only calculate speed when our position changes within the same piece of track
            last_distance = self.last_position['piecePosition']['inPieceDistance']
            new_distance = self.position['piecePosition']['inPieceDistance']
            self.last_speed = self.speed
            self.speed = new_distance - last_distance

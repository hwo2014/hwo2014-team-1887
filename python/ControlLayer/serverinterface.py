import json
import socket
import sys
import botstate
import time
import re
import random
import csv


class ServerInterface(object):

    def __init__(self, socket, bot, botstate, competitorStates):

        self.name = __name__

        import logging
        self.logger = logging.getLogger(self.name)

        self.log = True
        if self.log:
            self.commslog = open('comms.log', 'wb')
            target = open('throttlelog.csv', 'wb')
            self.throttlelog = csv.writer(target)
            self.throttlelog.writerow(['Tick', 'RequestedSpeed', 'CurrentSpeed', 'CurrentThrottle'])

        self.dump = False

        self.message_sent = False
        self.socket = socket
        self.bot = bot
        self.bot_state = botstate
        self.competitor_states = competitorStates
        self.tick = 0
        self.game_init = False

        self.floored = False
        self.started = time.time()
        self.race = {}

        self.seen_msg_types = []
        self.handler_suffixes = {}
        self.before_handlers = {}
        self.bot_handlers = {}
        self.after_handlers = {}
        self.first_cap_re = re.compile('(.)([A-Z][a-z]+)')
        self.all_cap_re = re.compile('([a-z0-9])([A-Z])')

        self.game_start_received = False

        if 'on_all_messages' in dir(self.bot):
            self.bot_handlers['all_messages'] = getattr(self.bot, 'on_all_messages')

        self.logger.debug('%s init done', self.name)

    def convert_camel_case(self, input):
        s1 = self.first_cap_re.sub(r'\1_\2', input)
        return self.all_cap_re.sub(r'\1_\2', s1).lower()

    def pp(self, data):
        import pprint
        pp = pprint.PrettyPrinter(indent=4)
        self.logger.debug(pp.pformat(data))

    def msg_loop(self):
        """ server interface message loop
             1/ receives messages and dispatches them to callback methods
             2/ generates on_tick callbacks when the server tick changes
        :return:
        """

        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            # message received

            lastTick = self.tick

            if self.dump:
                self.logger.debug("Received: %s", line.strip())
            if self.log:
                self.commslog.write('RECV ' + line)

            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']

            if 'gameTick' in msg:
                self.tick = msg['gameTick']

            if self.bot_state.game_id == '' and 'gameId' in msg:
                self.bot_state.game_id = msg['gameId']
                self.logger.info('Game ID %s', self.bot_state.game_id)

            # generate message map for this type of message, if it's the first time we've seen it
            if not msg_type in self.seen_msg_types:
                self.seen_msg_types.append(msg_type)
                handler_suffix = self.convert_camel_case(msg_type)
                self.handler_suffixes[msg_type] = handler_suffix

                if 'before_' + handler_suffix in dir(self):
                    self.before_handlers[msg_type] = getattr(self, 'before_' + handler_suffix)

                if 'on_' + handler_suffix in dir(self.bot):
                    self.bot_handlers[msg_type] = getattr(self.bot, 'on_' + handler_suffix)

                if 'after_' + handler_suffix in dir(self):
                    self.after_handlers[msg_type] = getattr(self, 'after_' + handler_suffix)

            # dispatch message to whoever wants to know
            handled = False
            if msg_type in self.before_handlers:
                self.before_handlers[msg_type](data)
                handled = True
            if 'all_messages' in self.bot_handlers:
                self.bot_handlers['all_messages'](self.handler_suffixes[msg_type])
            if msg_type in self.bot_handlers:
                self.bot_handlers[msg_type]()
                handled = True
            # make on_tick callbacks if this message incremented the tick counter (probable)
            if self.tick != lastTick:
                try:
                    self.bot.on_tick(self.tick)
                except AttributeError:
                    pass
            if msg_type in self.after_handlers:
                self.after_handlers[msg_type](data)
                handled = True
            else:
                self.do_ping()

            # emit a warning when a message is received that doesn't have a handler
            if not handled:
                self.logger.warning("Received unhandled message {0}".format(msg_type))
                self.pp(msg)

            # wait for the next message from the server - blocks the thread
            line = socket_file.readline()

        self.logger.debug("Connection closed server side!")

    def before_turbo_available(self, data):
        self.bot_state.turbo = data
        self.bot_state.turbo_available = True

    def stash_position_data(self, data, aBot=None):
        if aBot is None:
            aBot = self.bot_state

        aBot.update_position(data)

    def CalculateDampedThrottle(self, target_speed, current_speed, last_throttle):
        lower = min(target_speed, current_speed)
        upper = max(target_speed, current_speed)

        if target_speed == 0:
            return 0
        elif current_speed == 0:
            return 1

        clossness_factor = lower / upper

        if current_speed < target_speed:
            if clossness_factor <= 0.9999:
                return 1
        elif current_speed > target_speed:
            if clossness_factor <= 0.9999:
                return 0

        return target_speed / 10.0

    def before_car_positions(self, data):
        if self.game_start_received:
            for carData in data:
                if carData['id']['name'] == self.bot_state.name and carData['id']['color'] == self.bot_state.color:
                    self.stash_position_data(carData)
                else:
                    self.stash_position_data(carData, self.competitor_states[carData['id']['name'] + carData['id']['color']])
        else:
            for carData in data:
                if carData['id']['name'] == self.bot_state.name and carData['id']['color'] == self.bot_state.color:
                    continue

                self.competitor_states[carData['id']['name'] + carData['id']['color']] = botstate.botState(self.bot, carData['id']['name'], '', carData['id']['color'])

    def after_car_positions(self, data):

        # Don't attempt to race before the race has started
        if not self.game_start_received:
            self.do_ping()
            return

        # Consider activating turbo
        if self.bot_state.use_turbo is True:
            quote = random.choice([
                'Once this baby hits 88 miles per hour ...',    # Back to the Future
                'Punch It, Chewie',                             # Star Wars
                'Warp speed ahead Mr Scott',                    # Star Trek
                'Make it so',                                   # Star Trek
                'Jump!',                                        # Battlestar Galactica
                'To infinity and beyond',                       # Buzz Lightyear, Toy Story
                'So long, and thanks for all the fish',         # The Hitchhiker's Guide to the Galaxy
                'Don\'t Panic',                                 # The Hitchhiker's Guide to the Galaxy
                'Once more unto the breach',                    # Henry V by William Shakespeare
                'Engaging super pursuit mode',                  # Knight Rider
                'It\'s Turbo Time!',                            # Jingle All the Way (1996)
                'Turbo-Tastic!'                                 # Wreck It Ralph
            ])
            self.logger.info("Activating turbo: %s", quote)
            self.bot_state.turbo_available = False
            self.bot_state.turbo = False
            self.msg('turbo', quote)
            return

        # Consider switching lanes
        if self.bot_state.lane != self.bot_state.target_lane:
            # we are not in the target lane
            if self.bot_state.pending_switch_to_lane != self.bot_state.target_lane:
                # we aren't waiting to switch to the target lane

                if self.bot_state.target_lane > self.bot_state.lane:
                    # we want to go positive (Right)

                    if self.bot_state.pending_switch_to_lane != self.bot_state.lane + 1:
                        # we aren't currently waiting for the switch we need
                        self.bot_state.pending_switch_to_lane = self.bot_state.lane + 1
                        self.do_switch_lane('Right')
                        return

                else:
                    # we want to go negative (Left)

                    if self.bot_state.pending_switch_to_lane != self.bot_state.lane - 1:
                        # we aren't currently waiting for the switch we need
                        self.bot_state.pending_switch_to_lane = self.bot_state.lane - 1
                        self.do_switch_lane('Left')
                        return

        # Consider changing the throttle
        if not self.bot_state.speed is None and not self.bot_state.target_speed is None:
            new_throttle = self.CalculateDampedThrottle(self.bot_state.target_speed, self.bot_state.speed, self.bot_state.last_throttle)
            if self.log:
                self.throttlelog.writerow([str(self.tick), str(self.bot_state.target_speed), str(self.bot_state.speed), str(new_throttle)])
            self.bot_state.last_throttle = new_throttle
            self.do_throttle_adjustment(self.bot_state.last_throttle)
            return

        # if not doing anything else .. send a ping
        self.do_ping()

    def before_join_race(self, data):
        self.logger.debug('Joined')

    def stash_dimensions_data(self, data, aBot=None):
        if aBot is None:
            aBot = self.bot_state

        self.logger.debug(data)

        if data['id']['color'] == aBot.color:
            aBot.update_dimensions(data)

    def before_game_init(self, data):
        self.logger.debug('Game init')
        self.race = data['race']
        for aCar in data['race']['cars']:
            self.stash_dimensions_data(aCar)
        self.game_init = True

    def before_lap_finished(self, data):
        if data['car']['name'] == self.bot_state.name:
            self.bot_state.lap += 1
        else:
            for aKey, aCar in self.competitor_states.iteritems():
                if data['car']['name'] == aCar.name:
                    aCar.lap += 1
                    break

    def before_finish(self, data):
        if data['name'] == self.bot_state.color:
            self.game_start_received = False

    def before_dnf(self, data):
        if data['car']['name'] == self.bot_state.name:
            self.game_start_received = False
        else:
            del self.competitor_states[data['car']['name'] + data['car']['color']]

    def before_game_end(self, data):
        self.game_start_received = False
        self.bot_state = botstate.botState(self, self.bot_state.name, self.bot_state.key, 'None')
        self.competitor_states = {}

    def before_tournament_end(self, data):
        self.game_start_received = False

    def before_your_car(self, data):
        self.logger.info('Received my car data; I am %s and my color is %s', data['name'], data['color'])
        self.bot_state.color = data['color']

    def after_game_start(self, data):
        self.logger.debug('Race started')
        self.game_start_received = True
        self.do_ping()

    def before_crash(self, data):
        if data['color'] == self.bot_state.color:
            self.bot_state.crashed = self.tick
            self.logger.info("I (%s) crashed at tick %s (my speed was %s)", data['color'], self.tick, self.bot_state.speed)
        else:
            self.logger.debug("%s (%s) crashed at tick %s", data['name'], data['color'], self.tick)

    def before_spawn(self, data):
        if data['color'] == self.bot_state.color:
            self.bot_state.spawned = self.tick
            self.logger.info("I (%s) spawned at tick %s", data['color'], self.tick)
        else:
            self.logger.debug("%s (%s) spawned at tick %s", data['name'], data['color'], self.tick)

    def before_error(self, data):
        self.logger.error("Error: {0}".format(data))

    def msg(self, msg_type, data=None):
        payload = {'msgType': msg_type}

        if not data is None:
            payload['data'] = data

        self.send(json.dumps(payload))

    def send(self, msg):
        if self.dump:
            self.logger.debug("Sending: %s", msg)
        if self.log:
            self.commslog.write('SEND ' + msg + "\n")
        self.socket.sendall(msg + "\n")
        self.message_sent = True

    def do_join_race_advanced(self, cars=None, track=None, password=None):
        self.do_advanced_join('joinRace', cars, track, password)

    def do_create_race(self, cars=None, track=None, password=None):
        self.do_advanced_join('createRace', cars, track, password)

    def do_advanced_join(self, msg_type, cars=None, track=None, password=None):
        data = {"botId": {"name": self.bot_state.name, "key": self.bot_state.key}}

        if not cars is None:
            data['carCount'] = int(cars)
        if not track is None:
            data['trackName'] = track
        if not password is None:
            data['password'] = password

        self.msg(msg_type, data)

    def do_simple_join(self):
        data = {'name': self.bot_state.name, 'key': self.bot_state.key}

        self.msg('join', data)

    def before_create_race(self, data):
        self.logger.info("Race Created!")

    def do_ping(self):
        self.msg("ping")

    def do_switch_lane(self, direction):
        self.logger.info('switching lane %s', direction)
        return self.msg("switchLane", direction)

    def do_throttle_adjustment(self, throttle):
        if throttle > 1:
            throttle = 1
        if throttle < 0:
            throttle = 0
        if self.game_start_received == True:
            self.msg("throttle", throttle)
        else:
            self.msg("throttle", 0)

import pickle
import json
import pprint
import os
import sys

if len(sys.argv) == 3:
    json_filename = sys.argv[1]
    pkl_filename = sys.argv[2]

else:
    print "Usage: ./script file.json file.pkl"
    exit()

#######################################################################################################################
# Read and output the file

print "Using json file", json_filename
print "Using pickle file", pkl_filename

pp = pprint.PrettyPrinter(indent=4)

json_file = open(json_filename, 'rb')
json_contents = json_file.read()
json_data = json.loads(json_contents)
json_file.close()

pkl_file = open(pkl_filename, 'rb')
pkl_contents = pickle.load(pkl_file)
pkl_file.close()

number_segments = len(pkl_contents['pieces'][0]['lanes'][0]['maximum_speeds'])
segment = 0

print "-" * 72

message_types = ['lapFinished', 'gameEnd', 'fullCarPositions', 'turboStart', 'turboEnd']

color = 'red'


last_index = -1
speed = 0.0
turbo = False

for msg in json_data:

    if not msg['msgType'] in message_types:
        continue

    print msg['msgType'].ljust(18),

    if 'gameTick' in msg:
        print 'tick =', msg['gameTick'],

    if msg['msgType'] == 'lapFinished':
        print 'lap =', msg['data']['lapTime']['lap'],
        print 'time =', msg['data']['lapTime']['millis'], 'ms',

    if msg['msgType'] == 'gameEnd':
        for lap in msg['data']['bestLaps']:
            if lap['car']['color'] == color:
                print 'best lap =', lap['result']['lap'],

    if msg['msgType'] == 'fullCarPositions':
        for pos in msg['data']:
            if pos['id']['color'] == color:

                index = pos['piecePosition']['pieceIndex']
                distance = pos['piecePosition']['inPieceDistance']
                current_lane = pos['piecePosition']['lane']['endLaneIndex']

                if index == last_index:
                    speed = distance - last_distance

                last_index = index
                last_distance = distance

                print 'index =', index,
                print 'distance =', distance,
                print 'segment =', segment,
                print 'lane =', current_lane,
                print 'speed =', speed,
                print 'a =', pos['angleOffset'],

                piece = pkl_contents['pieces'][index]
                lane = piece['lanes'][current_lane]

                if not turbo:
                    lane['maximum_speed_hint'] = speed
                    lane['learned_maximum_speed'] = None
                    del lane['learned_maximum_speed']

                """
                length = lane['length']
                segment = int((float(distance) / float(length)) * int(number_segments))
                if segment >= number_segments:
                    segment = number_segments - 1
                maximum_speed = lane['maximum_speeds'][int(segment)]
                """


    if msg['msgType'] == 'turboStart' or msg['msgType'] == 'turboEnd':
        print '=' * 90,
        turbo = not turbo

    if turbo:
        print 'turbo',
    else:
        print 'normal',

    print

#print pp.pformat(data)
print "-" * 72

pkl_file = open(pkl_filename, 'wb')
pickle.dump(pkl_contents, pkl_file)
pkl_file.close()
